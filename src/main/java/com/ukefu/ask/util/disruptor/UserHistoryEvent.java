package com.ukefu.ask.util.disruptor;

import com.ukefu.ask.web.model.UserHistory;

public class UserHistoryEvent {
	private long id ;

	private UserHistory history ;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public UserHistory getHistory() {
		return history;
	}

	public void setHistory(UserHistory history) {
		this.history = history;
	}
	
}
