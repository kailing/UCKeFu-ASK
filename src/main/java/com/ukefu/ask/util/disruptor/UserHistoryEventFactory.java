package com.ukefu.ask.util.disruptor;

import com.lmax.disruptor.EventFactory;

public class UserHistoryEventFactory implements EventFactory<UserHistoryEvent>{

	@Override
	public UserHistoryEvent newInstance() {
		return new UserHistoryEvent();
	}
}
