package com.ukefu.ask.util.disruptor;

import com.lmax.disruptor.RingBuffer;
import com.ukefu.ask.web.model.UserHistory;

public class UserHistoryEventProducer {
	private final RingBuffer<UserHistoryEvent> ringBuffer;

    public UserHistoryEventProducer(RingBuffer<UserHistoryEvent> ringBuffer)
    {
        this.ringBuffer = ringBuffer;
    }

    public void onData(UserHistory history)
    {
        long id = ringBuffer.next();  // Grab the next sequence
        try{
        	UserHistoryEvent event = ringBuffer.get(id);
        	event.setHistory(history);
        }finally{
            ringBuffer.publish(id);
        }
    }
}
